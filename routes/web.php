<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;

Route::get('/test', function() {
    return Hash::make('sindipns');
});

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('dosen', 'DosenController');
    Route::resource('kelas', 'KelasController');
    Route::resource('matakuliah', 'MatakuliahController');
    Route::resource('mahasiswa', 'MahasiswaController');
    Route::resource('kehadiran', 'KehadiranController');
    Route::get('/laporankehadiran', 'KehadiranController@KehadiranExcel');
    Route::resource('status', 'StatusController');
    Route::resource('nilai', 'NilaiController');
    Route::get('/laporannilai', 'NilaiController@NilaiExcel');
    Route::get('nilai/pdf/cetak', 'NilaiController@print')->name('nilai.pdf.cetak');
});
