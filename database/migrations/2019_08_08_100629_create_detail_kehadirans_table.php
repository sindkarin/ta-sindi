<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailKehadiransTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('detail_kehadiran', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('kehadiran_id');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('mahasiswa_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('detail_kehadiran');
    }
}
