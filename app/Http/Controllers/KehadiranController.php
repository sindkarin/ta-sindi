<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kehadiran;
use App\Dosen;
use App\Mahasiswa;
use App\Status;
use App\DetailKehadiran;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\KehadiranExport;

class KehadiranController extends Controller
{
    use TraitMessage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kehadiran::get();

        return view('kehadiran.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $method = 'create';
        $dosen = Dosen::pluck('nama', 'id');
        $mahasiswa = Mahasiswa::pluck('nama', 'id');
        $status = Status::pluck('nama', 'id');

        return view('kehadiran.create', compact('method', 'dosen', 'mahasiswa', 'status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'tanggal',
            'jam',
            'dosen_mk_id',
        ]);

        $kehadiran = Kehadiran::create($data);
        $detail = [
            'kehadiran_id' => $kehadiran->id,
            'status_id' => $request->get('status_id'),
            'mahasiswa_id' => $request->get('mahasiswa_id'),
        ];
        DetailKehadiran::create($detail);
        $this->message();

        return redirect('kehadiran');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $view = [
            'method' => 'edit',
            'item' => Kehadiran::findOrFail($id),
            'mahasiswa' => Mahasiswa::pluck('nama', 'id'),
            'status' => Status::pluck('nama', 'id'),
            'dosen' => Dosen::pluck('nama', 'id'),
        ];

        return view('kehadiran.create')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only([
            'tanggal',
            'jam',
            'dosen_mk_id',
        ]);

        Kehadiran::findOrFail($id)->update($data);

        $detail = [
            'kehadiran_id' => $id,
            'mahasiswa_id' => $request->get('mahasiswa_id'),
        ];
        $isi_detail = [
            'status_id' => $request->get('status_id'),
        ];
        $this->message();
        DetailKehadiran::createOrUpdate($detail, $isi_detail);

        return redirect('kehadiran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kehadiran::findOrFail($id)->delete();
        $this->message(1);

        return redirect()->back();
    }


    public function rules()
    {
        $rules=[
        'tanggal'       =>'required',
        'jam'           =>'required',
        'dosen_mk_id'   =>'required',
        'status_id'     =>'required',
        'mahasiswa_id'  =>'required',
        ];
    }

    public function KehadiranExcel(Request $request)
    {
        return Excel::download(new KehadiranExport, 'kehadiran.xlsx');    
     }
}
