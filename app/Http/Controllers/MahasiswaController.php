<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mahasiswa;
use App\Kelas;

class MahasiswaController extends Controller
{
    use TraitMessage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Mahasiswa::get();

        return view('mahasiswa.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $method = 'create';
        $kelas = Kelas::pluck('nama', 'id');

        return view('mahasiswa.create', compact('method', 'kelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        Mahasiswa::create($data);
        $this->message();

        return redirect('mahasiswa');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $view = [
            'method' => 'edit',
            'kelas' => Kelas::pluck('nama', 'id'),
            'item' => Mahasiswa::findOrFail($id),
        ];

        return view('mahasiswa.create')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        Mahasiswa::findOrFail($id)->update($data);

        $this->message();

        return redirect('mahasiswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Mahasiswa::findOrFail($id)->delete();
        $this->message(1);

        return redirect()->back();
    }

    public function rules()
    {
        $rules=[
        'nim'           =>'required',
        'nama'          =>'required',
        'jk'            =>'required',
        'kelas_id'      =>'required',
        ];
    }
}
