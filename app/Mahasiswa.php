<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    protected $fillable = [
        'nim',
        'nama',
        'jk',
        'kelas_id',
];
public $timestamps = false;
public function Mahasiswa()
{
   
    return $this->hasMany(Mahasiswa::class);
}
}
