<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = 'dosen';
    protected $fillable = [
        'nip',
        'nama',
        'nohp',
        'user_id',
];
public $timestamps = false;
public function Nilai()
{
   
    	return $this->hasMany(Nilai::class);
}
}
