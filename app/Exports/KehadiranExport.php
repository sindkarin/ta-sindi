<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Kehadiran;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;


class KehadiranExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Kehadiran::all();
    }

     public function headings(): array
    {
        return [
            '#',
            'Tanggal',
            'Jam',
            'Dosen',
            'Created at',
            'Updated at'
        ];
    }

     public function registerEvents(): array
    {
        return [
                AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
