<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Nilai;

class NilaiExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
     public function collection()
    {
        return Nilai::all();
    }

     public function headings(): array
    {
        return [
            '#',
            'Quiz 1',
            'Quiz 2',
            'UTS',
            'UAS',
            'SEMESTER',
            'TAHUN',
            'MAHASISWA',
            'DOSEN',
            'Created at',
            'Updated at'
        ];
    }

     public function registerEvents(): array
    {
        return [
                AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
