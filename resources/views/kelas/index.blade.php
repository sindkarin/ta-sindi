@extends('layouts.basic')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Data Kelas
            </div>
            <div class="card-body">
                <a href="{{ route("kelas.create") }}" class="btn btn-primary mb-4">Tambah Data +</a>
               <table class="table table-bordered text-center">
                        <tr>
                            <td>No</td>
                            <td>Nama</td>
                            <td>Angkatan</td>
                            <td>Aksi</td>
                        </tr>
                        @foreach($data as $item)
                            <tr>
                               <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->angkatan }}</td>
                                <td class="text-center">
                                  <a href="{{ url('kelas/'.$item->id.'/edit') }}" class="btn btn-success btn-md"><i class="fa fa-edit"></i></a>
                                	<form action="{{ url('/kelas', ['id' => $item->id]) }}" method="post" class="btn btn-danger btn-sm">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
