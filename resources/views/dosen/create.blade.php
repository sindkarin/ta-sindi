@extends('layouts.basic')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
        @if($method == 'create')
                <form method="post" action="{{ url('dosen/') }}">
            @else
                {!! Form::model($item, ['url' => [url('dosen')."/".$item->id], 'method' => 'Put']) !!}
            @endif
          <div class="card">
            <div class="card-header bg-primary text-white">
                TAMBAH DOSEN
            </div>
            <div class="card-body">
                  @csrf
                  @include('dosen._form')
            </div>
        </div>
         </form>
        </div>
    </div>
</div>  
    
@endsection