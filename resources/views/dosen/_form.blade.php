<div class="form-group{{ $errors->has('nip') ? 'has-error' : '' }} ">
    {!! Form::label('nip', 'NIP', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('nip', null, ['class'=>'form-control','id' => 'nip', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('nip', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('nama') ? 'has-error' : '' }} ">
    {!! Form::label('nama', 'Nama', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('nama', null, ['class'=>'form-control','id' => 'nama', 'autofocus', 'tabindex' => '2']) !!}
        {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('nohp') ? 'has-error' : '' }} ">
    {!! Form::label('nohp', 'NO Hp', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('nohp', null, ['class'=>'form-control','id' => 'nohp', 'autofocus', 'tabindex' => '3']) !!}
        {!! $errors->first('nohp', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('user_id') ? 'has-error' : '' }} ">
    {!! Form::label('user_id', 'Userlogin', ['class'=>'col-md-6 control-label']) !!}
    <div class="col-md-12">
        {!! Form::select('user_id', $user, null, ['class'=>'form-control','id' => 'user_id', 'autofocus', 'tabindex' => '3']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-6">
        {!! Form::submit('Simpan', ['class'=>'btn btn-primary btn-simpan','tabindex' => '7']) !!}
       <a href="{{ route('dosen.index') }}" class="btn btn-secondary">Kembali</a>
    </div>
</div>
