@extends('layouts.basic')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
        @if($method == 'create')
                <form method="post" action="{{ url('status/') }}">
            @else
                {!! Form::model($item, ['url' => [url('status')."/".$item->id], 'method' => 'Put']) !!}
            @endif
          <div class="card">
            <div class="card-header bg-primary text-white">
                Tambah Status
            </div>
            <div class="card-body">
                  @csrf
                  @include('status._form')
            </div>
        </div>
         </form>
        </div>
    </div>
</div>  
    
@endsection