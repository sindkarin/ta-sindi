<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sistem Informasi Absen & Nilai </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-primary shadow-sm" style="padding:15px">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Sistem Informasi Absen & Nilai
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        @role('admin')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('dosen') }}">{{ __('Dosen') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('kelas') }}">{{ __('Kelas') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('matakuliah') }}">{{ __('Matakuliah') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('mahasiswa') }}">{{ __('Mahasiswa') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('kehadiran') }}">{{ __('Kehadiran') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('status') }}">{{ __('Status') }}</a>
                        </li>
                        @else
                         <li class="nav-item ml-auto">
                            <a class="nav-link" href="{{ url('nilai') }}">{{ __('Nilai') }}</a>
                        </li>

                        @endrole
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    <div class="col-md-12">
        <div class="py-3">
            @include('layouts._flash')
            @yield('content')
        </div>
    </div>
    </div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/laravel.js') }}"></script>
@yield('js')
</body>
</html>
