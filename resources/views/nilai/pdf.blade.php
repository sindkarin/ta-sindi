
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nilai Mahasiswa
                <div class="card-body">
                    <table width="100%" class="table table-bordered" border="1">
                        <tr>
                            <td width="1%">No</td>
                            <td>Nama Mahasiswa</td>
                            <td>Matakuliah</td>
                            <td>Semester</td>
                            <td>Tahun</td>
                            <td>Quiz 1</td>
                            <td>Quiz 2</td>
                            <td>Uts</td>
                            <td>Uas</td>
                            <td>Nilai Akhir</td>
                        </tr>
                        @foreach($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->mahasiswa->nama }}</td>
                                <td>{{ $item->dosenMatkul ? $item->dosenMatkul->matakuliah->nama : '-' }}</td>
                                <td>{{ $item->semester }}</td>
                                <td>{{ $item->tahun }}</td>
                                <td>{{ $item->quiz1 }}</td>
                                <td>{{ $item->quiz2 }}</td>
                                <td>{{ $item->uts }}</td>
                                <td>{{ $item->uas }}</td>
                                <td>
                                    @php
                                        $q1 = $item->quiz1 * 0.1;
                                        $q2 = $item->quiz2 * 0.1;
                                        $uts = $item->uts * 0.35;
                                        $uas = $item->uas * 0.40;
                                        $kehadiran = 5;
                                        $total = ($q1 + $q2 + $uts + $uas + $kehadiran);
                                    @endphp
                                    {{ $total }}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
