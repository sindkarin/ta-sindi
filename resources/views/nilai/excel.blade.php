<table>
    <thead>
        <tr>
             <td>No</td>
            <td>Quiz 1</td>
            <td>Quiz 2</td>
            <td>Uts</td>
            <td>Uas</td>
            <td>Semester</td>
            <td>Tahun</td>
            <td>Mahasiswa</td>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->quiz1 }}</td>
            <td>{{ $item->quiz2 }}</td>
            <td>{{ $item->uts }}</td>
            <td>{{ $item->uas }}</td>
            <td>{{ $item->semester }}</td>
            <td>{{ $item->tahun }}</td>
            <td>{{ $item->Mahasiswa->nama }}</td>
        </tr>
    @endforeach
    </tbody>
</table>