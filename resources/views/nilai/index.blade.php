@extends('layouts.basic')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Data Nilai
            </div>
            <div class="card-body">
                <a href="{{ route("nilai.create") }}" class="btn btn-primary mb-4">Tambah Data +</a>
               <table class="table table-bordered text-center">
                        <tr>
                            <td>No</td>
                            <td>Quiz 1</td>
                            <td>Quiz 2</td>
                            <td>Uts</td>
                            <td>Uas</td>
                            <td>Semester</td>
                            <td>Tahun</td>
                            <td>Mahasiswa</td>
                            <td>Aksi</td>
                        </tr>
                        @foreach($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->quiz1 }}</td>
                                <td>{{ $item->quiz2 }}</td>
                                <td>{{ $item->uts }}</td>
                                <td>{{ $item->uas }}</td>
                                <td>{{ $item->semester }}</td>
                                <td>{{ $item->tahun }}</td>
                                <td>{{ $item->Mahasiswa->nama }}</td>
                                <td class="text-center">
                                  <a href="{{ url('nilai/'.$item->id.'/edit') }}" class="btn btn-success btn-md"><i class="fa fa-edit"></i></a>
                                	<form action="{{ url('/nilai', ['id' => $item->id]) }}" method="post" class="btn btn-danger btn-sm">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
