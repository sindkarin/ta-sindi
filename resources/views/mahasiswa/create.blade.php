@extends('layouts.basic')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
        @if($method == 'create')
                <form method="post" action="{{ url('mahasiswa/') }}">
            @else
                {!! Form::model($item, ['url' => [url('mahasiswa')."/".$item->id], 'method' => 'Put']) !!}
            @endif
          <div class="card">
            <div class="card-header bg-primary text-white">
                Tambah Mahasiswa
            </div>
            <div class="card-body">
                  @csrf
                  @include('mahasiswa._form')
            </div>
        </div>
         </form>
        </div>
    </div>
</div>  
    
@endsection