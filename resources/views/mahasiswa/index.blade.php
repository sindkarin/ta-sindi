@extends('layouts.basic')

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Mahasiswa
                <div class="card-body">
                    <a href="{{ url('mahasiswa/create')}}" class="btn btn-primary">Tambah Data</a>
                    <br><br>
                    <table class="table table-bordered">
                        <tr>
                            <td>No</td>
                            <td>NIM</td>
                            <td>Nama</td>
                            <td>Jenis Kelamin</td>
                            <td>Aksi</td>
                        </tr>
                        @foreach($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->nim }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jk }}</td>
                                <td>
                                    <a href="{{ url('mahasiswa/'.$item->id.'/edit') }}" class="btn btn-block btn-warning">Edit</a>
                                    <form action="{{ url('/mahasiswa', ['id' => $item->id]) }}" method="post">
                                        <input class="btn btn-danger btn-block" type="submit" value="Hapus" />
                                        @method('delete')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="container">
    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Data Mahasiswa
            </div>
            <div class="card-body">
                <a href="{{ route("mahasiswa.create") }}" class="btn btn-primary mb-4">Tambah Data +</a>
               <table class="table table-bordered text-center">
                        <tr>
                           <td>No</td>
                            <td>NIM</td>
                            <td>Nama</td>
                            <td>Jenis Kelamin</td>
                            <td>Aksi</td>
                        </tr>
                        @foreach($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->nim }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jk }}</td>
                                <td class="text-center">
                                  <a href="{{ url('mahasiswa/'.$item->id.'/edit') }}" class="btn btn-success btn-md"><i class="fa fa-edit"></i></a>
                                	<form action="{{ url('/mahasiswa', ['id' => $item->id]) }}" method="post" class="btn btn-danger btn-sm">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
